###DNS LOG
* [dnstap](http://dnstap.info/)
* [Passive DNS Collection and Analysis The 'dnstap' Approach](http://www.fbcinc.com/e/CIF/presentations/Vixie_Passive_DNS_Collection_Construction_and_Use.pdf)
* [DSC: A DNS STATISTICS COLLECTOR](http://dns.measurement-factory.com/tools/dsc/)
* [DNSTOP: STAY ON TOP OF YOUR DNS TRAFFIC](http://dns.measurement-factory.com/tools/dnstop/)   不支持tcp
* [Monitoring DNS Queries with tcpdump](http://jontai.me/blog/2011/11/monitoring-dns-queries-with-tcpdump/)
* [Tcpdump patterns](https://wiki.tyk.nu/index.php?title=Tcpdump_patterns#Matching_DNS_Traffic)



###参考资料
* [Comparison and Analysis of Managed DNS Provider](http://blog.cloudharmony.com/2012/08/comparison-and-analysis-of-managed-dns.html)
###开源软件
####解析软件
* BIND
* KNOT
* NSD
####管理软件
* [NicTool](https://github.com/msimerson/NicTool)
* 

* [What DNS Is Not
PAUL VIXIE, INTERNET SYSTEMS CONSORTIUM
DNS IS MANY THINGS TO MANY PEOPLE—PERHAPS TOO MANY THINGS TO TOO MANY PEOPLE.](http://queue.acm.org/detail.cfm?id=1647302)