##VIM的配置文件




###基本配置
```

" 显示行号
set nu
" 高亮当前行
set cursorline
" 用空格代替Tab
set expandtab
" 自动缩进
set autoindent
set smartindent
set smarttab
set cindent
" 缩进宽度
set tabstop=4
set shiftwidth=4
" 语法高亮
syntax on
" 禁止在Makefile 中将Tab 转换成空格
autocmd FileType make set noexpandtab

```

###BUNDLE安装配置
BUNDLE首先是VIM的一个插件,提供了VIM的插件统一管理功能.
####安装
```
git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
```
####vimrc配置
```
""""""""""""""""" BUNDLE start ###################
set nocompatible               " be iMproved
filetype off    	" required start

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
" original repos on github
Bundle 'tpope/vim-fugitive' 
Bundle 'Lokaltog/vim-easymotion'  
" non github repos
"Bundle 'git://git.wincent.com/command-t.git'  
Bundle 'https://github.com/Lokaltog/vim-powerline.git'
" vim-scripts repos
Bundle 'L9'  
Bundle 'taglist.vim'  
Bundle 'The-NERD-tree'
Bundle 'winmanager'
Bundle 'minibufexpl.vim'
Bundle 'bufexplorer.zip'
" Bundle 'OmniCppComplete'

filetype plugin indent on     " required! end


" vim-powerline config
set laststatus=2
set t_Co=256
let g:Powerline_symbols = 'unicode'
set encoding=utf8

" taglist config
let Tlist_Show_Menu = 1
let Tlist_Show_One_File = 1
```

###参考资料
* https://github.com/qyuhen/book/blob/master/C%20%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0.pdf
* [VIM 管理好插件，神器也可以华丽～](http://www.cnblogs.com/respawn/archive/2012/08/21/2649483.html)
* [VIM常用操作, 插件和vimrc文件](http://www.cnblogs.com/moodlxs/archive/2012/03/24/2415526.html)