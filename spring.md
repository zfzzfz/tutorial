####参考资料
* [spring Modules 模块](https://github.com/waylau/spring-framework-4-reference/blob/master/I.%20Overview%20of%20Spring%20Framework/2.2.%20Modules.md)


####web.xml
```xml
<context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>
            classpath*:/spring/applicationContext.xml,
            classpath*:/spring/applicationContext-shiro.xml
        </param-value>
    </context-param>
    <context-param>
        <param-name>spring.profiles.default</param-name>
        <param-value>produce</param-value>
    </context-param>
 
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
 
    <filter>
        <filter-name>encodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
 
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath*:/spring/spring-mvc.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>    
   
  <filter>
    <filter-name>shiroFilter</filter-name>
    <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    <init-param>
      <param-name>targetFilterLifecycle</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>shiroFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
   
  <listener>
    <listener-class>
      org.springframework.web.context.request.RequestContextListener
    </listener-class>
  </listener>
 
    <session-config>
        <session-timeout>30</session-timeout>
    </session-config>    
   
    <error-page>
        <exception-type>java.lang.Throwable</exception-type>
        <location>/WEB-INF/view/error/500.html</location>
    </error-page>
    <error-page>
        <error-code>500</error-code>
        <location>/WEB-INF/view/error/500.html</location>
    </error-page>
    <error-page>
        <error-code>404</error-code>
        <location>/WEB-INF/view/error/404.html</location>
    </error-page>
```
###applicationContext.xml
```xml
<!-- 使用annotation 自动注册bean,并检查@Required,@Autowired的属性已被注入 -->
  <context:component-scan base-package="com.minyond.modules">
    <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    <context:exclude-filter type="annotation" expression="org.springframework.web.bind.annotation.ControllerAdvice"/>
  </context:component-scan>
   
  <!-- MyBatis配置 -->
  <bean id="sqlSessionFactory" class="com.minyond.core.common.mybatis.SqlSessionFactoryBean">
    <property name="dataSource" ref="dataSource" />
    <!-- 自动扫描entity目录, 省掉Configuration.xml里的手工配置 -->
    <property name="typeAliasesPackage" value="com.minyond.modules.*.entity"/>
    <!-- 显式指定Mapper文件位置 -->
    <property name="mapperLocations" value="classpath:/mybatis/com/minyond/modules/**/mapper/*Mapper.xml"/>
  </bean>
  <!-- 扫描basePackage下所有以@MyBatisRepository标识的 接口-->
  <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="com.minyond.modules" />
    <property name="annotationClass" value="com.minyond.core.common.mybatis.MyBatisRepository"/>
  </bean>
   
  <!-- 事务管理器配置 -->
  <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <property name="dataSource" ref="dataSource" />
  </bean>
 
  <!-- 使用annotation定义事务 -->
  <tx:annotation-driven transaction-manager="transactionManager" proxy-target-class="true" /> 
 
  <!-- produce环境 -->
  <beans profile="produce">
    <context:property-placeholder ignore-resource-not-found="true"
      location="classpath*:/jdbc.properties" />  
 
    <!-- 数据源配置,使用应用内的DBCP数据库连接池 -->
    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close">
      <!-- Connection Info -->
      <property name="driverClassName" value="${jdbc.driver}" />
      <property name="url" value="${jdbc.url}" />
      <property name="username" value="${jdbc.username}" />
      <property name="password" value="${jdbc.password}" />
      <property name="defaultAutoCommit" value="false" />      
    </bean>
  </beans>
   
  <!-- production环境 -->
  <beans profile="production">
    <context:property-placeholder ignore-resource-not-found="true"
      location="classpath*:/jdbc.properties" />  
 
    <!-- 数据源配置,使用应用内的Tomcat JDBC连接池 -->
    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource" destroy-method="close">      
       <property name="driverClass" value="${jdbc.driver}"></property>
       <property name="jdbcUrl" value="${jdbc.url}"></property>
       <property name="user" value="${jdbc.username}"></property>
       <property name="password" value="${jdbc.password}"></property>
       
       <!-- 详细参数说明参见database-config.properties -->
       <property name="initialPoolSize" value="${c3p0.initialPoolSize}"></property>
       <property name="minPoolSize" value="${c3p0.minPoolSize}"></property>
       <property name="maxPoolSize" value="${c3p0.maxPoolSize}"></property>
       <property name="maxIdleTime" value="${c3p0.maxIdleTime}"></property>
       <property name="acquireIncrement" value="${c3p0.acquireIncrement}"></property>
       <property name="idleConnectionTestPeriod" value="${c3p0.idleConnectionTestPeriod}"></property>
       <property name="acquireRetryAttempts" value="${c3p0.acquireRetryAttempts}"></property>
       <property name="breakAfterAcquireFailure" value="${c3p0.breakAfterAcquireFailure}"></property>
       <property name="maxStatements" value="${c3p0.maxStatements}"></property>
       <property name="testConnectionOnCheckout" value="${c3p0.testConnectionOnCheckout}"></property>
 
    </bean>
  </beans>
```

####spring-shiro.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd"
    default-lazy-init="true">
 
    <description>Shiro安全配置</description>
 
    <!-- Shiro's main business-tier object for web-enabled applications -->
    <bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
        <property name="realm" ref="shiroRealm" />
    </bean>
 
    <!-- 項目自定义的Realm -->
    <bean id="shiroRealm" class="com.minyond.modules.base.system.realm.ShiroRealm" depends-on="userDao">
        <property name="userService" ref="userService"/>
    </bean>
     
    <!-- Shiro Filter -->
    <bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
        <property name="securityManager" ref="securityManager" />
        <property name="loginUrl" value="/" />
        <property name="successUrl" value="/main" />
        <property name="filterChainDefinitions">
            <value>
              <!--静态资源直接通过-->
        /libs/** = anon
                /login = anon
                /test = anon
                /logout = logout
                /** = authc
            </value>
        </property>
    </bean>
     
    <!-- 保证实现了Shiro内部lifecycle函数的bean执行 -->
    <bean id="lifecycleBeanPostProcessor" class="org.apache.shiro.spring.LifecycleBeanPostProcessor"/>
     
</beans>
```