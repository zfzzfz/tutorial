日志
-----------

###日志层级
* 服务器日志 如网络\
* 代理日志,如httpd,nginx,tomcat的日志
* 应用软件日志 程序本身的输出

###日志分类
* 业务日志
* 安全日志
* 访问日志
* 调试日志
* 错误日志

###TODO
* 基于Kafka的日志系统

###参考资料
* [开源日志系统比较](http://www.gaizaoren.org/archives/491)
* [ apache kafka消息服务](http://blog.csdn.net/lizhitao/article/details/23743821)
* [The Log: What every software engineer should know about real-time data's unifying abstraction](http://engineering.linkedin.com/distributed-systems/log-what-every-software-engineer-should-know-about-real-time-datas-unifying)