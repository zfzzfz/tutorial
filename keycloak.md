

### 项目
* [mcloud](https://github.com/heyuxian/mcloud)
* [keycloak开发者指南](https://gitee.com/itmuch/spring-cloud-yes/blob/master/doc/keycloak-learn/Keycloak%E6%90%AD%E5%BB%BA%E6%89%8B%E6%8A%8A%E6%89%8B%E6%93%8D%E4%BD%9C%E6%8C%87%E5%8D%97.md)


### 资料
* [user-storage-spi](https://www.keycloak.org/docs/latest/server_development/index.html#_user-storage-spi)
* [How to integrate or make use of KeyCloak user database in my application?](https://stackoverflow.com/questions/39259607/how-to-integrate-or-make-use-of-keycloak-user-database-in-my-application)
* [use-keycloaks-users-in-own-application](https://stackoverflow.com/questions/46836248/use-keycloaks-users-in-own-application)