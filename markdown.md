### Markdown简介

> Markdown 是一种轻量级标记语言，它允许人们使用易读易写的纯文本格式编写文档，然后转换成格式丰富的HTML页面。    —— [维基百科](https://zh.wikipedia.org/wiki/Markdown)

正如您在阅读的这份文档，它使用简单的符号标识不同的标题，将某些文字标记为**粗体**或者*斜体*，下面列举了几个高级功能。 

#### 代码块
``` python
@requires_authorization
def somefunc(param1='', param2=0):
    '''A docstring'''
    if param1 > param2: # interesting
        print 'Greater'
    return (param2 - param1 + 1) or None
class SomeClass:
    pass
>>> message = '''interpreter
... prompt'''
```

#### LaTex 公式
$$    x = \dfrac{-b \pm \sqrt{b^2 - 4ac}}{2a} $$

#### 表格
| Item      |    Value | Qty  |
| :-------- | --------:| :--: |
| Computer  | 1600 USD |  5   |
| Phone     |   12 USD |  12  |
| Pipe      |    1 USD | 234  |

### 参考
#Standard Markdown

##Strong and Emphasize
```
*emphasize*   **strong**
_emphasize_   __strong__
```
result:
> *emphasize*   **strong**
> _emphasize_   __strong__

##Links and Email
####Inline:
```
An [example](http://url.com/ "Title")
```
result: 
> An [example](http://url.com/ "Title")


####Reference-style labels (titles are optional):
```
An [example][id]. Then, anywhere
else in the doc, define the link:

  [id]: http://example.com/  "Title"
```
result:
> An [example][id]. Then, anywhere
> else in the doc, define the link:
> 
> [id]: http://example.com/  "Title"

####Email:
```
An email <example@example.com> link.
```
result: 
> An email <example@example.com> link.

##Images
####Inline (titles are optional):
```
![alt text](/path/img.jpg "Title")
```
result: 
> ![alt text](/path/img.jpg "Title")

####Reference-style:
```
![alt text][id]
[id]: /url/to/img.jpg "Title"
```
result:
> ![alt text][id]
> [id]: /url/to/img.jpg "Title"

##Headers
#### Setext-style:

```
Header 1
========

Header 2
--------
```
result: 
> Header 1
> ========
> 
> Header 2
> --------

#### atx-style (closing #’s are optional):
```
# Header 1 #

## Header 2 ##

###### Header 6
```
result: 
> # Header 1 #
> 
> ## Header 2 ##
> 
> ###### Header 6

##Lists
#### Ordered, without paragraphs:
```
1.  Foo
2.  Bar
```
result: 
> 1.  Foo
> 2.  Bar

####Unordered, with paragraphs:
```
*   A list item.

    With multiple paragraphs.

*   Bar
```
result: 
> *   A list item.
> 
>  With multiple paragraphs.
> 
> *   Bar

#### You can nest them:
```
*   Abacus
    * answer
*   Bubbles
    1.  bunk
    2.  bupkis
        * BELITTLER
    3. burper
*   Cunning
```
result:
> *   Abacus
>     * answer
> *   Bubbles
>     1.  bunk
>     2.  bupkis
>         * BELITTLER
>     3. burper
> *   Cunning

##Blockquotes
```
> Email-style angle brackets
> are used for blockquotes.

> > And, they can be nested.

> #### Headers in blockquotes
> 
> * You can quote a list.
> * Etc.
```
result: 

> Email-style angle brackets
> are used for blockquotes.

> > And, they can be nested.

> #### Headers in blockquotes
> 
> * You can quote a list.
> * Etc.

##Inline Code
```
`<code>` spans are delimited
by backticks.

You can include literal backticks
like `` `this` ``.
```
result:
> `<code>` spans are delimited
> by backticks.
> 
> You can include literal backticks
> like `` `this` ``.

##Block Code
Indent every line of a code block by at least 4 spaces or 1 tab.
```
This is a normal paragraph.

    This is a preformatted
    code block.
```
result:
> This is a normal paragraph.
> 
>     This is a preformatted
>     code block.

##Horizontal Rules
Three or more dashes or asterisks:
```
---

* * *

- - - -
```
result: 
> ---
> 
> * * *
> 
> - - - -
##Hard Line Breaks
End a line with two or more spaces:
```
Roses are red,   
Violets are blue.
```
result:
> Roses are red,   
> Violets are blue.