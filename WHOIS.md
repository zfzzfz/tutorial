##WHOIS
###概述
registry(注册局)==>registrar(注册商)==>registrant

ICANN规定注册局和注册商必须提供whois服务.


###架构模式

####THIN
在注册局只保留域名的主要信息,不保存联系人的主要信息,主要

####THICK

###消息格式
#### NewGtld格式
> ```
> Domain Name: tmall.wang
> Domain ID: 20140620g10001g-10332554
> WHOIS Server: 61.139.126.52
> Referral URL: www.west263.com
> Updated Date: 
> Creation Date: 2014-06-20T01:22:02Z
> Registry Expiry Date: 2015-06-20T01:22:02Z
> Sponsoring Registrar: 成都西维数码
> Sponsoring Registrar IANA ID: 1556
> Domain Status: ok
> Registrant ID: 1556-295865-d-01
> Registrant Name: wu wen long
> Registrant Organization: wu wen long
> Registrant Street: Hang Zhou Shi Xiao Shan Qu Yun
> Registrant City: Hang Zhou Shi
> Registrant State/Province: ZJ
> Registrant Postal Code: 310052
> Registrant Country: cn
> Registrant Phone: +86.13958176030
> Registrant Phone Ext: 
> Registrant Fax: +86.13958176030
> Registrant Fax Ext: 
> Registrant Email: dongsheng329@163.com
> Admin ID: 1556-295865-a-01
> Admin Name: wenlong wu
> Admin Organization: wenlong wu
> Admin Street: Hang Zhou Shi Xiao Shan Qu Yun
> Admin City: Hang Zhou Shi
> Admin State/Province: ZJ
> Admin Postal Code: 310052
> Admin Country: cn
> Admin Phone: +86.13958176030
> Admin Phone Ext: 
> Admin Fax: +86.13958176030
> Admin Fax Ext: 
> Admin Email: dongsheng329@163.com
> Tech ID: 1556-295865-t-01
> Tech Name: wenlong wu
> Tech Organization: wenlong wu
> Tech Street: Hang Zhou Shi Xiao Shan Qu Yun
> Tech City: Hang Zhou Shi
> Tech State/Province: ZJ
> Tech Postal Code: 310052
> Tech Country: cn
> Tech Phone: +86.13958176030
> Tech Phone Ext: 
> Tech Fax: +86.13958176030
> Tech Fax Ext: 
> Tech Email: dongsheng329@163.com
> Name Server: ns5.myhostadmin.net
> Name Server: ns6.myhostadmin.net
> DNSSEC: unsigned
> >>> Last update of WHOIS database: 2014-07-21T04:37:55Z <<<
> 
> NOTICE: The expiration date displayed in this record is the date the
> registrar's sponsorship of the domain name registration in the registry is
> currently set to expire. This date does not necessarily reflect the expiration
> date of the domain name registrant's agreement with the sponsoring
> registrar.  Users may consult the sponsoring registrar's Whois database to
> view the registrar's reported date of expiration for this registration.
> 
> TERMS OF USE: The information in the Whois database is collected through ICANN-accredited registrars. Zodiac Holdings Limited and its affiliates （“We”） make this information available to you “as is” and do not guarantee its accuracy or completeness. <br/ >By submitting a Whois query, you agree that you will use this data only for lawful purposes and that, under no circumstances will you use this data： （1） to allow, enable, or otherwise support the transmission of mass unsolicited, commercial advertising or solicitations via direct mail, electronic mail, or by telephone;  （2） in contravention of any applicable data and privacy protection laws; or  （3） to enable high volume, automated, electronic processes that apply to our registry （or our systems）. Compilation, repackaging, dissemination, or other use of the Whois database in its entirety, or of a substantial portion thereof, is not allowed without our prior written permission.  
> 
> ```

| 字段名      |    备注 | 
| :-------- | :--------:|
|Domain Name |域名|
|Name Server |DNS服务器（解析服务器，在万网注册时推荐使用默认DNS）|
|Registrant ID |注册人ID|
|Registrant Name |注册人姓名（联系信息请保证准确可用）|
|Registrant Organization |注册人单位|
|Registrant Address |注册人地址|
|Registrant City |注册人城市|
|Registrant Province/State |注册人省/州|
|Registrant Postal Code |注册人邮编|
|Registrant Country Code |注册人国家代码|
|Registrant Phone Number | 注册人电话号码 （联系信息请保证准确可用）|
|Registrant Fax |注册人传真|
|Registrant Email |注册人电子邮箱（联系信息请保证准确可用）|
|Technical ID |技术联系人ID|
|Technical Name |技术联系人姓名|
|Technical Organization |技术联系人单位|
|Technical Address |技术联系人地址|
|Technical City |技术联系人城市|
|Technical Province/State | 技术联系人省/州|
|Technical Postal Code |技术联系人邮编|
|Technical Country Code |技术联系人国家代码|
|Technical Phone Number |技术联系人电话号码|
|Technical Fax |技术联系人传真|
|Technical Email |技术联系人电子邮件|
|Administrative ID |管理联系人ID|
|Administrative Name |管理联系人姓名|
|Administrative Organization |管理联系人单位|
|Administrative Address |管理联系人地址|
|Administrative City |管理联系人城市|
|Administrative Province/State |管理联系人省/州|
|Administrative Postal Code |管理联系人邮编|
|Administrative Country Code |管理联系人国家代码|
|Administrative Phone Number |管理联系人电话号码|
|Administrative Fax |管理联系人传真|
|Administrative Email |管理联系人电子邮箱|
|Billing ID |付费联系人ID|
|Billing Name |付费联系人|
|Billing Organization |付费联系人单位|
|Billing Address |付费联系人地址|
|Billing City |付费联系人城市|
|Billing Province/State |付费联系人省/州|
|Billing Postal Code |付费联系人邮编|
|Billing Country Code |付费联系人国家代码|
|Billing Phone Number |付费联系人电话号码|
|Billing Fax |付费联系人传真|
|Billing Email |付费联系人电子邮箱|
|Expiration Date |域名到期日|


###CNNIC域名格式
> ```
> Domain Name: xn--xkRy9Kk1Bz66A.xn--fiQs8S    xn--psSu7C921AfvU.xn--fiQs8S	
> Domain Status: ok
> Registrant ID: zzbb001011
> Registrant: 清华大学
> Registrant Contact Email: shibj@chinagov.cn
> Registrar Name: 中央编办事业发展中心（原中央编办机关服务局事业发展中心）
> Name Server1: ns3.chinaorg.cn
> Name Server2: ns4.chinaorg.cn
> Create Date: 2000-11-06 00:00:00
> Expiration Date: 2016-10-12 15:38:08
> DNSSEC: unsigned
> ```

| 字段名      |    备注 | 
| :-------- | :--------:|
|Domain Name|域名|
|ROID|注册识别码|
|Domain Status|域名状态|
|Registrant ID|注册人ID|
|Registrant Organization|注册人单位|
|Registrant Name|注册人姓名（联系信息请保证准确可用）|
|Registrant Email|注册人电子邮箱（联系信息请保证准确可用）|
|Sponsoring Registrar|所属注册商|
|Name Server|DNS服务器|
|Registration Date|域名注册日期|
|Expiration Date|域名到期日|
|Dnssec Deployment|DNS安全扩展|


### 参考资料

####标准
* [WHOIS wiki](http://en.wikipedia.org/wiki/WHOIS)
* [FC 3912 – WHOIS protocol specification](https://tools.ietf.org/html/rfc3912)
* [ICANN whois](http://whois.icann.org/)
* [域名注册信息含义](http://www.net.cn/service/faq/yuming/ymzc/201108/4968.html)

####开源实现
* [robowhois](https://www.robowhois.com)